#!/usr/bin/env python3

# PROBLEM DESCRIPTION
# If we list all the natural numbers below 10 that 
# are multiples of 3 or 5, we get 3, 5, 6 and 9. 
# The sum of these multiples is 23.

#Find the sum of all the multiples of 3 or 5 below 1000.

def sum_of_multiples(x):
    return sum([i for i in range(0, x) if (i % 3 == 0) or (i % 5 == 0)])

print("Sum of multiples of 3 and 5 up to 1000 is {}".format(sum_of_multiples(1000)))
