#!/usr/bin/env python3

# PROBLEM DESCRIPTION
# Each new term in the Fibonacci sequence is generated by adding the 
# previous two terms. By starting with 1 and 2, the first 10 terms will be:

#    1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

# By considering the terms in the Fibonacci sequence whose values do not exceed 
# four million, find the sum of the even-valued terms.

import itertools

def fibo():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b

if __name__ == "__main__":
    four_milly = itertools.takewhile(lambda n: n <= 4000000, fibo())
    print(sum(x for x in four_milly if x % 2 == 0))
