#!/usr/bin/env python3

# A palindromic number reads the same both ways. 
# The largest palindrome made from the product of
# two 2-digit numbers is 9009 = 91 × 99.
#
# Find the largest palindrome made from the product
# of two 3-digit numbers.

from itertools import product

def max_three_digit_palindrome():
    return max((a * b for a, b in product(range(100,1000), repeat=2) \
                if ((str(a * b) == str(a * b)[::-1]))))

print("The largest palindrome number made from the product of two 3-digit numbers is {}".format(max_three_digit_palindrome()))
