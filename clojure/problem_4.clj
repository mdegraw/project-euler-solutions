; A palindromic number reads the same both ways. 
; The largest palindrome made from the product of
; two 2-digit numbers is 9009 = 91 × 99.
;
; Find the largest palindrome made from the product
; of two 3-digit numbers.

(defn palindrome? [n]
  (=  (seq (str n)) (reverse (seq (str n)))))

(def three-digit-products
  (for [n (range 100 1000) m (range 100 1000)] (* n m)))

(def max-palindrome 
  (reduce max (filter palindrome? three-digit-products)))

(println (str "Max palindrome " max-palindrome))
