sumOfMultiples :: Integer -> Integer
sumOfMultiples x = sum [x | x <- [0,1..x-1], ((x `mod` 3 == 0) || (x `mod` 5 == 0))]

main =  do 
	let n = sumOfMultiples 1000
	print n
